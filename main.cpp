/*
    Game 2048.cpp using class to simplify access and make code clearer
    Alojz Soltys 2020
*/

#include <iostream>
#include <time.h>
#include <conio.h>

using namespace std;

class Game2048
{
public:
    Game2048();
    ~Game2048(){}
    void moveUpdate(bool);
    void newNumberGenerator();

    //control functions
    bool winningCondition(bool &);
    bool loosingCondition();

    // keyboard functions
    bool moveUp();
    bool moveDown();
    bool moveRight();
    bool moveLeft();
    void stepBack();
protected:
    // protected functions
    void displayGame();
    int newNumberPosition();
    int newNumberValue();

    //protected variables
    unsigned int previousMoveGameBoard [4][4];
    unsigned int tempGameBoard[4][4];
    unsigned int gameBoard[4][4];

    unsigned int previousMoveScore;
    unsigned int totalScore;
    unsigned int pointAddition;
};

int main()
{
    srand(time(NULL)); // random generator seed
    enum arrow_keys {KEY_UP = 72, KEY_DOWN = 80, KEY_LEFT = 75, KEY_RIGHT = 77};
    bool movePossible;
    bool winningFlag = false;
    cout << "\n\n\tPress any key to start new game\n\n";
    getch();
    // creating new game
    Game2048 * pGame = new Game2048;

    while (1){ // run mode
        int c = getch(); //arrow keys send two numbers from which first is the same

        switch(c){
        case KEY_UP:    movePossible = pGame->moveUp();
                        break;
        case KEY_DOWN:  movePossible = pGame->moveDown();
                        break;
        case KEY_RIGHT: movePossible = pGame->moveRight();
                        break;
        case KEY_LEFT:  movePossible = pGame->moveLeft();
                        break;
        default:        if ((char)c == 'e'){        // end game
                            cout << "\n\n\t---- Program terminated ----\n\n";
                            delete pGame;
                            return 0;
                        }
                        else if ((char)c == 'n'){   // game restart
                            delete pGame;
                            system("cls");
                            Game2048 * pGame = new Game2048;
                        }
                        else if ((char)c == 'b'){   // step back
                            pGame->stepBack();
                        }
                        break;
        }

        // arrow keys send two numbers from which only second is in use. This condition ignores first one
        if ((c == KEY_UP || c == KEY_DOWN || c == KEY_RIGHT || c == KEY_LEFT)){
            pGame->moveUpdate(movePossible);

            // after each possible move, check for win or lose condition
            if (pGame->winningCondition(winningFlag))
                cout << "\n\n\t----- Victory is yours! -----\n\n";
            else if (pGame->loosingCondition())
                cout << "\n\n\t-----   Game over! :(   -----\n\n";
        }
    }
    return 0;
}


Game2048::Game2048()
{
    // creating blank gaming board
    totalScore = 0;
    previousMoveScore = 0;
    for (int i = 0; i < 4; i++)
        for (int j = 0; j < 4; j++)
            gameBoard[i][j] = 0;

    // generate two random numbers and creating backup
    newNumberGenerator();
    newNumberGenerator();

    for (int i = 0; i < 4; i++)
        for (int j = 0; j < 4; j++)
            previousMoveGameBoard[i][j] = tempGameBoard[i][j] = gameBoard[i][j];
    displayGame();
}

void Game2048::displayGame()
{
    system("cls");
    cout << "Press 'b' to step back, 'e' to end program or 'n' for a new game\n";
    cout << "\n\tYour score: " << totalScore << "\n\n";
    for(int y = 0; y < 4; y++){
        cout << "\t";
        for (int x = 0; x < 4; x++){
            if (gameBoard[x][y] == 0)
                cout << ".\t";
            else
                cout << gameBoard[x][y] << "\t";
        }
    cout << "\n\n";
    }
}

int Game2048::newNumberPosition()
{
    unsigned int *ptr = &gameBoard[0][0];
    int temp[16] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
    int counter = 0;

    //creating temp array of empty places
    for (int x = 0; x < 16; x++){
        if (*(ptr + x) == 0){
            temp[counter] = x;
            counter++;
        }
    }
    return temp[rand() % (counter)];
}

int Game2048::newNumberValue()
{
    int probability = rand() % 10;
    if(probability == 9)
        return 4;
    else
        return 2;
}

void Game2048::newNumberGenerator()
{
    int newPosition = newNumberPosition();
    int newValue = newNumberValue();
    unsigned int *pGameBoard = &gameBoard[0][0];
    *(pGameBoard + newPosition) = newValue;
}

bool Game2048::moveUp()
{
    int emptyPlace;
    unsigned int lastNonZeroNumber;
    bool movedU = false;
    pointAddition = 0;

    for(int x = 0; x < 4; x++){
        emptyPlace = 0;
        lastNonZeroNumber = 0;
        for(int y = 0; y < 4; y++){
            if (tempGameBoard[x][y] == 0){
                emptyPlace++;
            }
            else if (tempGameBoard[x][y] != 0 && lastNonZeroNumber == 0){
                lastNonZeroNumber = tempGameBoard[x][y];
                if (emptyPlace != 0){
                    tempGameBoard[x][y-emptyPlace] = tempGameBoard[x][y];
                    tempGameBoard[x][y] = 0;
                    movedU = true;
                }
            }
            else if (tempGameBoard[x][y] != 0 && lastNonZeroNumber != 0){
                if(tempGameBoard[x][y] == lastNonZeroNumber){
                    emptyPlace++;
                    tempGameBoard[x][y-emptyPlace] *= 2;
                    pointAddition += tempGameBoard[x][y-emptyPlace];
                    tempGameBoard[x][y] = 0;
                    lastNonZeroNumber = 0;
                    movedU = true;
                }
                else {
                    lastNonZeroNumber = tempGameBoard[x][y];
                    if(emptyPlace != 0){
                        tempGameBoard[x][y-emptyPlace] = tempGameBoard[x][y];
                        tempGameBoard[x][y] = 0;
                        movedU = true;
                    }
                }
            }
        }
    }
    if (movedU)
        return true;
    else
        return false;
}

bool Game2048::moveDown()
{
    int emptyPlace;
    unsigned int lastNonZeroNumber;
    bool movedD = false;
    pointAddition = 0;

    for(int x = 0; x < 4; x++){
        emptyPlace = 0;
        lastNonZeroNumber = 0;
        for(int y = 3; y >= 0; y--){
            if (tempGameBoard[x][y] == 0){
                emptyPlace++;
            }
            else if (tempGameBoard[x][y] != 0 && lastNonZeroNumber == 0){
                lastNonZeroNumber = tempGameBoard[x][y];
                if (emptyPlace != 0){
                    tempGameBoard[x][y+emptyPlace] = tempGameBoard[x][y];
                    tempGameBoard[x][y] = 0;
                    movedD = true;
                }
            }
            else if (tempGameBoard[x][y] != 0 && lastNonZeroNumber != 0){
                if(tempGameBoard[x][y] == lastNonZeroNumber){
                    emptyPlace++;
                    tempGameBoard[x][y+emptyPlace] *= 2;
                    pointAddition += tempGameBoard[x][y+emptyPlace];
                    tempGameBoard[x][y] = 0;
                    lastNonZeroNumber = 0;
                    movedD = true;
                }
                else {
                    lastNonZeroNumber = tempGameBoard[x][y];
                    if(emptyPlace != 0){
                        tempGameBoard[x][y+emptyPlace] = tempGameBoard[x][y];
                        tempGameBoard[x][y] = 0;
                        movedD = true;
                    }
                }
            }
        }
    }
    if (movedD)
        return true;
    else
        return false;
}

bool Game2048::moveRight()
{
    int emptyPlace;
    unsigned int lastNonZeroNumber;
    bool movedR = false;
    pointAddition = 0;

    for(int y = 0; y < 4; y++){
        emptyPlace = 0;
        lastNonZeroNumber = 0;
        for(int x = 3; x >= 0; x--){
            if (tempGameBoard[x][y] == 0){
                emptyPlace++;
            }
            else if (tempGameBoard[x][y] != 0 && lastNonZeroNumber == 0){
                lastNonZeroNumber = tempGameBoard[x][y];
                if (emptyPlace != 0){
                    tempGameBoard[x+emptyPlace][y] = tempGameBoard[x][y];
                    tempGameBoard[x][y] = 0;
                    movedR = true;
                }
            }
            else if (tempGameBoard[x][y] != 0 && lastNonZeroNumber != 0){
                if(tempGameBoard[x][y] == lastNonZeroNumber){
                    emptyPlace++;
                    tempGameBoard[x+emptyPlace][y] *= 2;
                    pointAddition += tempGameBoard[x+emptyPlace][y];
                    tempGameBoard[x][y] = 0;
                    lastNonZeroNumber = 0;
                    movedR = true;
                }
                else {
                    lastNonZeroNumber = tempGameBoard[x][y];
                    if(emptyPlace != 0){
                        tempGameBoard[x+emptyPlace][y] = tempGameBoard[x][y];
                        tempGameBoard[x][y] = 0;
                        movedR = true;
                    }
                }
            }
        }
    }
    if (movedR)
        return true;
    else
        return false;
}

bool Game2048::moveLeft()
{
    int emptyPlace;
    unsigned int lastNonZeroNumber;
    bool movedL = false;
    pointAddition = 0;

    for(int y = 0; y < 4; y++){
        emptyPlace = 0;
        lastNonZeroNumber = 0;
        for(int x = 0; x < 4; x++){
            if (tempGameBoard[x][y] == 0){
                emptyPlace++;
            }
            else if (tempGameBoard[x][y] != 0 && lastNonZeroNumber == 0){
                lastNonZeroNumber = tempGameBoard[x][y];
                if (emptyPlace != 0){
                    tempGameBoard[x-emptyPlace][y] = tempGameBoard[x][y];
                    tempGameBoard[x][y] = 0;
                    movedL = true;
                }
            }
            else if (tempGameBoard[x][y] != 0 && lastNonZeroNumber != 0){
                if(tempGameBoard[x][y] == lastNonZeroNumber){
                    emptyPlace++;
                    tempGameBoard[x-emptyPlace][y] *= 2;
                    pointAddition += tempGameBoard[x-emptyPlace][y];
                    tempGameBoard[x][y] = 0;
                    lastNonZeroNumber = 0;
                    movedL = true;
                }
                else {
                    lastNonZeroNumber = tempGameBoard[x][y];
                    if(emptyPlace != 0){
                        tempGameBoard[x-emptyPlace][y] = tempGameBoard[x][y];
                        tempGameBoard[x][y] = 0;
                        movedL = true;
                    }
                }
            }
        }
    }
    if (movedL)
        return true;
    else
        return false;
}

void Game2048::moveUpdate(bool mPossible)
{
    // if move was possible, gameBoard goes to previous move and temporary game array is set to gameBoard, same with score
    if (mPossible){
        for (int i = 0; i < 4; i++){
            for (int j = 0; j < 4; j++){
                previousMoveGameBoard[i][j] = gameBoard[i][j];
                gameBoard[i][j] = tempGameBoard[i][j];
            }
        }
        previousMoveScore = totalScore;
        totalScore += pointAddition;
        newNumberGenerator();

        // preparation for next move
        for (int i = 0; i < 4; i++){
            for (int j = 0; j < 4; j++)
                tempGameBoard[i][j] = gameBoard[i][j];
    }
    displayGame();
    }
    else
        cout << "Cannot move this way\n";
}

bool Game2048::winningCondition(bool &vFlag)
{
    const unsigned int winNumber = 2048;

    //purpose of wFlag is to run this code only once
    if(vFlag == false){
        for (int i = 0; i < 4; i++){
            for (int j = 0; j < 4; j++){
                if(winNumber == gameBoard[i][j]){
                    vFlag = true;
                    return true;
                }
            }
        }
    }
    return false;
}

bool Game2048::loosingCondition()
{
    int x;
    int y;

    // searching for empty places
    for (x = 0; x < 4; x++)
        for (y = 0; y < 4; y++)
            if(gameBoard[x][y] == 0)
                return false;

    // searching for horizontal or vertical pairs
    for (x = 0; x < 4; x++)
        for (y = 0; y < 3; y++)
            if (gameBoard[x][y] == gameBoard[x][y+1])
                return false;

    for (y = 0; y < 4; y++)
        for (x = 0; x < 3; x++)
            if (gameBoard[x][y] == gameBoard[x+1][y])
                return false;
    // game is lost
    return true;
}

void Game2048::stepBack()
{
    for (int i = 0; i < 4; i++){
        for (int j = 0; j < 4; j++){
            gameBoard[i][j] = previousMoveGameBoard[i][j];
            tempGameBoard[i][j] = previousMoveGameBoard[i][j];
        }
    }
    totalScore = previousMoveScore;
    displayGame();
}
